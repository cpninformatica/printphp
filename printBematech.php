<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Origin: *");

/* Change to the correct path if you copy this example! */
require __DIR__ . '/vendor/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

/**
 * Install the printer using USB printing support, and the "Generic / Text Only" driver,
 * then share it (you can use a firewall so that it can only be seen locally).
 *
 * Use a WindowsPrintConnector with the share name to print.
 *
 * Troubleshooting: Fire up a command prompt, and ensure that (if your printer is shared as
 * "Receipt Printer), the following commands work:
 *
 *  echo "Hello World" > testfile
 *  copy testfile "\\%COMPUTERNAME%\Receipt Printer"
 *  del testfile
 */
try {
	if (empty($_POST['data'])) {
		throw new Exception("Parâmetro 'data' não informado.");
	}
	if (empty($_POST['data']['impressora'])) {
		throw new Exception("Parâmetro 'impressora' não informado.");
	}
	if (empty($_POST['data']['empresa'])) {
		throw new Exception("Parâmetro 'empresa' não informado.");
	}
	if (empty($_POST['data']['pedido'])) {
		throw new Exception("Parâmetro 'pedido' não informado.");
	}
	if (empty($_POST['data']['produtos'])) {
		throw new Exception("Parâmetro 'produtos' não informado.");
	}
	
    // Enter the share name for your USB printer here    
	$connector = new WindowsPrintConnector($_POST['data']['impressora']);
	
    $printer = new Printer($connector);    
	
	$printer->setFont(Printer::FONT_B);	
	$printer->setFont(Printer::FONT_A);
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$printer->text($_POST['data']['empresa']['nome_fantasia']."\n");
	$printer->setFont(Printer::FONT_B);
	$printer->text($_POST['data']['empresa']['endereco1']."\n");
	$printer->text($_POST['data']['empresa']['endereco2']."\n\n");
	$printer->setJustification(Printer::JUSTIFY_LEFT);
	$printer->setEmphasis(true);
	$printer->text("Pedido: ".$_POST['data']['pedido']['id']."\n\n");
	$printer->setEmphasis(false);
	$printer->text("........................................................\n");
	$printer->text("Cod  Item                         R$ Unit.  Qtd   Subtot\n");
	$printer->text("........................................................\n");	
	
	foreach ($_POST['data']['produtos'] as $prod) {
		$cod = str_pad($prod['id'], 4, " ", STR_PAD_RIGHT)." ";
		$descricao = str_pad($prod['descricao'], 28, " ", STR_PAD_RIGHT)." ";
		$valor_unit = str_pad($prod['valor'], 8, " ", STR_PAD_LEFT)." ";
		$qtde = str_pad($prod['quantidade'], 4, " ", STR_PAD_LEFT)." ";
		$subtotal = str_pad($prod['subtotal'], 8, " ", STR_PAD_LEFT);
		
		$printer->text($cod.$descricao.$valor_unit.$qtde.$subtotal."\n");
	}	
	
	$subtotal = str_pad($_POST['data']['pedido']['subtotal'], 8, " ", STR_PAD_LEFT);
	$desconto = str_pad($_POST['data']['pedido']['desconto'], 8, " ", STR_PAD_LEFT);
	$total = str_pad($_POST['data']['pedido']['total'], 8, " ", STR_PAD_LEFT);
	
	$printer->text("........................................................\n");
	$printer->text("                                       SubTotal ".$subtotal."\n");	
	$printer->text("                                       Desconto ".$desconto."\n");	
	$printer->text("                                          Total ".$total."\n\n");
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$printer->text(date("d/m/Y H:i:s")."\n");
	$printer->text("GERCPN\n");
	$printer->setJustification(Printer::JUSTIFY_LEFT);	

    $printer->cut();
    
    // Close printer
    $printer->close();		
	
	$ret = [
		'status' => true,
		'message' => 'Impresso com sucesso'
	];
} catch (Exception $e) {    
	$ret = [
		'status' => false,
		'message' => 'Falha ao imprimir. '.$e->getMessage()
	];
} finally {
	echo json_encode($ret);
}


?>